﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace LolUniverse
{
    class Program
    {
        private static HttpClient _client;

        /// <summary>
        /// Point d'entrée du programme console
        /// </summary>
        /// <param name="args">Arguments éventuelles</param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            try
            {
                _client = new HttpClient(); //Création du client Http
                _client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json")); //Ajout du header spécifiant le contenu accepté
                _client.DefaultRequestHeaders.UserAgent.ParseAdd("LOL Universe Parser"); //Ajout d'un user agent (nécessaire afin d'être accepté)
                var stories = await GetStories("https://universe-meeps.leagueoflegends.com/v1/fr_fr/explore2/index.json");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Récupère l'ensemble des histoires se trouvant sur le site de League Of Legends
        /// </summary>
        /// <param name="url">Url de l'API permettant de récupérer la liste des histoires</param>
        /// <returns>Liste des histoires correctement formatées</returns>
        private static async Task<List<Story>> GetStories(string url)
        {
            var response = await _client.GetStringAsync(url); //Récupère le contenu JSON de la page
            var universe = JsonSerializer.Deserialize<UniverseJson>(response); //Transforme le JSON en objet UniverseJson
            var storyPreviews = universe.Modules.Where(x => x.Type == "story-preview" && !string.IsNullOrEmpty(x.Url)); //Récupère uniquement les éléments de type histoire
            var stories = new List<Story>();
            var tasks = new List<Task<Story>>();
            
            //Pour chaque lien 
            foreach (var storyPreview in storyPreviews)
            {
                //Récupère l'histoire à partir de l'url relative fournie
                tasks.Add(GetStory(storyPreview.Url)); //ajout des tâches à la liste de tâches
            }

            //Toutes les tâches sont exécutées en asynchrone afin de gagner énormément de temps
            //Tant que le nombre de taches dans la liste est supérieur à 0
            while (tasks.Count > 0)
            {
                var task = await Task.WhenAny(tasks); //récupère la première tâche terminée
                var story = await task; //récupère l'histoire
                if (story != null)
                {
                    stories.Add(story); //ajoute l'histoire à la liste des histoires
                }
                tasks.Remove(task); //retire la tâche terminée de la liste
            }
            return stories;
        }

        private static async Task<Story> GetStory(string relativePath)
        {
            try
            {
                var response = await _client.GetStringAsync(BuildStoryUrl(relativePath)); //Récupère le contenu JSON de la page
                var page = JsonSerializer.Deserialize<PageJson>(response); //Transforme le JSON en objet PageJson
                Console.WriteLine(page.Id); //Affiche sur la console l'ID de la page traitée
                return new Story //Retourne une nouvelle histoire remplie avec les informations récupérées
                {
                    Id = page.Id, //ID
                    Title = page.Story.Title, //Titre
                    Subtitle = page.Story.Subtitle, //Sous-titre
                    Content = page.Story.StorySections[0].StorySubsections[0].Content, //Contenu (avec balises html)
                    Url = "https://universe.leagueoflegends.com/fr_FR/story/" + page.Id //Url réel de la page
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString()); //En cas d'erreur, affiche celle-ci dans la console
            }
            return null; //retourne null si une erreur s'est produite
        }

        /// <summary>
        /// Construit l'url à partir du chemin relatif fourni
        /// </summary>
        /// <param name="relativePath">Url relative</param>
        /// <returns></returns>
        private static string BuildStoryUrl(string relativePath)
        {
            return string.Concat("https://universe-meeps.leagueoflegends.com/v1", relativePath, "/index.json");
        }
    }
}
