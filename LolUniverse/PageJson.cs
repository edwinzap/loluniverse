﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace LolUniverse
{
    public class PageJson
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("story")]
        public StoryJson Story { get; set; }
    }

    public class StoryJson
    {
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("subtitle")]
        public string Subtitle { get; set; }
        [JsonPropertyName("story-sections")]
        public List<StorySectionJson> StorySections { get; set; }
    }

    public class StorySectionJson
    {
        [JsonPropertyName("story-subsections")]
        public List<StorySubSectionJson> StorySubsections { get; set; }
    }

    public class StorySubSectionJson
    {
        [JsonPropertyName("content")]
        public string Content { get; set; }
    }
}
