﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LolUniverse
{
    public class Story
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
    }
}
