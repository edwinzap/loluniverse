﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace LolUniverse
{
    public class UniverseJson
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        
        [JsonPropertyName("locale")]
        public string Locale { get; set; }
        
        [JsonPropertyName("modules")]
        public List<ModuleJson> Modules { get; set; }

        internal object Where()
        {
            throw new NotImplementedException();
        }
    }

    public class ModuleJson
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [DataMember(Name ="description")]
        public string Description { get; set; }
       
        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("minutes-to-read")]
        public int MinutesToRead { get; set; }

        //[JsonPropertyName("release-date")]
        //public DateTime ReleaseDate { get; set; }
    }
}
